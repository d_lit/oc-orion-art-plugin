<?php namespace Dmtttvn\Orion;

use Lang;
use Carbon\Carbon;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Dmtttvn\Orion\Components\News'         => 'news',
            'Dmtttvn\Orion\Components\Portfolio'    => 'portfolio',
            'Dmtttvn\Orion\Components\Project'      => 'project',
        ];
    }

    public function registerSettings()
    {
    }

    public function __construct () {
        $localeCode = Lang::getLocale();
        Carbon::setLocale( $localeCode );
        setlocale( LC_TIME, $localeCode . '_' . strtoupper($localeCode) . '.UTF-8' );
    }
}
