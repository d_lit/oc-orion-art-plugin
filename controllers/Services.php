<?php namespace Dmtttvn\Orion\Controllers;

use Flash;
use BackendMenu;
use Backend\Classes\Controller;
use Dmtttvn\Orion\Models\Service as ServiceModel;

class Services extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\RelationController',
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $bodyClass = 'compact-container';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Dmtttvn.Orion', 'company', 'services');
    }

    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $serviceId) {
                if (!$service = ServiceModel::find($serviceId))
                    continue;

                $service->delete();
            }

            Flash::success('Выбранные Услуги были успешно удалены.');
        }

        return $this->listRefresh();
    }
}