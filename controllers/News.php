<?php namespace Dmtttvn\Orion\Controllers;

use Flash;
use BackendMenu;
use Carbon\Carbon;
use ApplicationException;
use Backend\Classes\Controller;
use Dmtttvn\Orion\Models\News as NewsModel;

class News extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $bodyClass = 'compact-container';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Dmtttvn.Orion', 'company', 'news');
    }

    public function index()
    {
        $this->vars['newsTotal'] = NewsModel::count();
        $this->vars['newsPublished'] = NewsModel::isPublished()->count();
        $this->vars['newsImportant'] = NewsModel::isImportant()->count();
        $this->vars['newsDrafts'] = $this->vars['newsTotal'] - $this->vars['newsPublished'];
        
        $this->vars['newsCurrentMonth'] = NewsModel::whereBetween('published_at', array(
                Carbon::now()->startOfMonth(),
                Carbon::now()->endOfMonth())
            )->count();
        $this->vars['newsLastMonth'] = NewsModel::whereBetween('published_at', array(
                Carbon::now()->subMonth()->startOfMonth(),
                Carbon::now()->startOfMonth())
            )->count();

        $this->asExtension('ListController')->index();
    }

    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $newsId) {
                if (!$news = NewsModel::find($newsId))
                    continue;

                $news->delete();
            }

            Flash::success('Выбранные Новости были успешно удалены.');
        }

        return $this->listRefresh();
    }
}