<?php namespace Dmtttvn\Orion\Controllers;

use Flash;
use BackendMenu;
use ApplicationException;
use Backend\Classes\Controller;
use Dmtttvn\Orion\Models\Role as RoleModel;
use Dmtttvn\Orion\Models\Comrade as ComradeModel;

class Comrades extends Controller
{    
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];

    public $listConfig = [
        'comrades' => 'config_comrades_list.yaml',
        'roles' => 'config_roles_list.yaml'
    ];
    public $formConfig = 'config_comrade_form.yaml';
    
    public $bodyClass = 'compact-container';

    public function __construct()
    {        
        if (post('role_mode')) {
            $this->formConfig = 'config_role_form.yaml';
        }

        parent::__construct();        
        BackendMenu::setContext('Dmtttvn.Orion', 'company', 'comrades');
    }

    public function index()
    {
        $this->asExtension('ListController')->index();
        $this->bodyClass = 'compact-container';
    }

    public function index_onDeleteComrades()
    {
        if (($checkedIds = post('checkedComrades')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $comradeId) {
                if (!$comrade = ComradeModel::find($comradeId))
                    continue;

                $comrade->delete();
            }

            Flash::success('Выбранные Сотрудники были успешно удалены.');
        }

        return $this->listRefresh();
    }

    public function index_onDeleteRoles()
    {
        if (($checkedIds = post('checkedRoles')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $roleId) {
                if (!$role = RoleModel::find($roleId))
                    continue;

                $role->delete();
            }

            Flash::success('Выбранные Роли были успешно удалены.');
        }

        return $this->listRefresh();
    }

    //
    // Role form
    //

    public function onCreateRoleForm()
    {
        $this->asExtension('FormController')->create();
        return $this->makePartial('create_role_form');
    }

    public function onUpdateRoleForm()
    {
        $this->asExtension('FormController')->update(post('record_id'));
        $this->vars['recordId'] = post('record_id');
        return $this->makePartial('update_role_form');
    }

    public function onCreateRole()
    {
        $this->asExtension('FormController')->create_onSave();
        return $this->listRefresh('roles');
    }

    public function onUpdateRole()
    {
        $this->asExtension('FormController')->update_onSave(post('record_id'));
        return $this->listRefresh('roles');
    }

    public function onDeleteRole()
    {
        $this->asExtension('FormController')->update_onDelete(post('record_id'));
        return $this->listRefresh('roles');
    }
}