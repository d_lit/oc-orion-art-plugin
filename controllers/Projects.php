<?php namespace Dmtttvn\Orion\Controllers;

use Flash;
use BackendMenu;
use Carbon\Carbon;
use Backend\Classes\Controller;
use Dmtttvn\Orion\Models\Block as BlockModel;
use Dmtttvn\Orion\Models\Project as ProjectModel;

class Projects extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\RelationController',
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $bodyClass = 'compact-container';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Dmtttvn.Orion', 'portfolio', 'projects');

        $this->addCss('/plugins/dmtttvn/orion/assets/css/richeditor-fix.css');
    }

    public function index()
    {
        $this->vars['projectsTotal'] = ProjectModel::count();
        $this->vars['projectsPublished'] = ProjectModel::isPublished()->count();
        $this->vars['projectsComplex'] = ProjectModel::isComplex()->count();
        $this->vars['projectsDrafts'] = $this->vars['projectsTotal'] - $this->vars['projectsPublished'];

        $this->vars['projectsCurrentMonth'] = ProjectModel::whereBetween('produced_at', array(
                Carbon::now()->startOfMonth(),
                Carbon::now()->endOfMonth())
            )->count();
        $this->vars['projectsLastMonth'] = ProjectModel::whereBetween('produced_at', array(
                Carbon::now()->subMonth()->startOfMonth(),
                Carbon::now()->startOfMonth())
            )->count();

        $this->asExtension('ListController')->index();
    }

    public function index_onDelete()
    {
        if (($checkedIds = post('checkedProjects')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $projectId) {
                if (!$project = ProjectModel::find($projectId))
                    continue;

                $project->delete();
            }

            Flash::success('Выбранные проекты были успешно удалены.');
        }

        return $this->listRefresh();
    }

    public function formExtendModel($model)
        {
            if ($this->formGetContext() === ProjectModel::FORMAT_SIMPLE && !$model->format) {
                $model->format = ProjectModel::FORMAT_SIMPLE;
            }
            if ($this->formGetContext() === ProjectModel::FORMAT_COMPLEX && !$model->format) {
                $model->format = ProjectModel::FORMAT_COMPLEX;
            }
            return $model;
        }
}