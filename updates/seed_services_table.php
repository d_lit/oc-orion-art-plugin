<?php namespace Dmtttvn\Orion\Updates;

use Faker;
use Seeder;
use Storage;
use Carbon\Carbon;
use System\Models\File;
use Dmtttvn\Orion\Models\Service;

class SeedServicesTable extends Seeder
{
    public function prepareVars()
    {
        $this->dtNow = Carbon::now();
        $this->faker = Faker\Factory::create('ru_RU');
        File::where('attachment_type', 'Dmtttvn\Orion\Models\Service')->delete();
    }

    public function run()
    {
        $this->prepareVars();
        $servicesDir = 'media/temp/photos';

        Service::create([
            'name'          => 'Мультимедийные шоу',
            'short_name'    => 'Мультимедиа',
            'slug'          => 'multimedia',
            'excerpt'   => $this->faker->sentence($nbWords = $this->faker->numberBetween(12,24)),
            'details'       => $this->faker->text($maxNbChars = $this->faker->numberBetween(2000,3000)),
            'status'        => 'active'
        ]);
        Service::create([
            'name'          => 'Пиротехнические шоу',
            'short_name'    => 'Пиротехника',
            'slug'          => 'pyrotechnics',
            'excerpt'   => $this->faker->sentence($nbWords = $this->faker->numberBetween(12,24)),
            'details'       => $this->faker->text($maxNbChars = $this->faker->numberBetween(2000,3000)),
            'status'        => 'active'
        ]);
        Service::create([
            'name'          => 'Спецэффекты',
            'short_name'    => '',
            'slug'          => 'special-effects',
            'excerpt'   => $this->faker->sentence($nbWords = $this->faker->numberBetween(12,24)),
            'details'       => $this->faker->text($maxNbChars = $this->faker->numberBetween(2000,3000)),
            'status'        => 'active'
        ]);
        Service::create([
            'name'          => 'Водные шоу',
            'short_name'    => '',
            'slug'          => 'water-shows',
            'excerpt'   => $this->faker->sentence($nbWords = $this->faker->numberBetween(12,24)),
            'details'       => $this->faker->text($maxNbChars = $this->faker->numberBetween(2000,3000)),
            'status'        => 'active'
        ]);
        Service::create([
            'name'          => 'Лазерные шоу',
            'short_name'    => 'Лазеры',
            'slug'          => 'lasers',
            'excerpt'   => $this->faker->sentence($nbWords = $this->faker->numberBetween(12,24)),
            'details'       => $this->faker->text($maxNbChars = $this->faker->numberBetween(2000,3000)),
            'status'        => 'active'
        ]);
        Service::create([
            'name'          => 'Дизайн',
            'short_name'    => '',
            'slug'          => 'design',
            'excerpt'   => $this->faker->sentence($nbWords = $this->faker->numberBetween(12,24)),
            'details'       => $this->faker->text($maxNbChars = $this->faker->numberBetween(2000,3000)),
            'status'        => 'active'
        ]);

        foreach (Service::all() as $service) {
            $this->attachRandomImage($service, 'cover', $servicesDir);
        }
    }

    public function attachRandomImage($model, $field, $directory)
    {
        $tempMediaImages = Storage::disk('local')->files($directory);

        $path = storage_path('app/' . $tempMediaImages[
            $this->faker->numberBetween(0, count($tempMediaImages) - 1)
        ]);

        $attach = new File;
        $attach->title = $this->faker->sentence($this->faker->numberBetween(3,5));
        $attach->description = $this->faker->sentence($this->faker->numberBetween(10,15));
        $attach->fromFile($path)->save();

        $model->$field()->add($attach);
    }
}