<?php namespace Dmtttvn\Orion\Updates;

use Faker;
use Seeder;
use Dmtttvn\Orion\Models\Role;

class SeedRolesTable extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');

        $roleNames = [
            'Арт директор',
            'Технический директор',
            'Генеральный директор',
            'Продюсер',
            'Дизайн и программирование пиротехнического шоу',
            'Дизайн и программирование лазерного шоу',
            'Дизайн и программирование водного фонтана',
            'Дизайн и программирование светового шоу',
            'Дизайн и программирование спецэфектов',
            'Концепт-дизайн и трехмерное моделирование',
            'Таймкод-синхронизация',
            'Музыкальная компиляция',
            'Фото',
            'Художник по костюму',
            'Постановщик трюков',
        ];

        for ($i = 0; $i < count($roleNames); $i++) { 
            Role::create([
                'name' => $roleNames[$i],
                'description' => $faker->sentence($nbWords = $faker->numberBetween(48,64))
            ]);
        }
    }
}