<?php namespace Dmtttvn\Orion\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDmtttvnOrionPeople extends Migration
{
    public function up()
    {
        Schema::create('dmtttvn_orion_comrades', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('role_id')->unsigned();
            $table->string('name');
            $table->text('profile')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('dmtttvn_orion_comrades');
    }
}