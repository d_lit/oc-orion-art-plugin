<?php namespace Dmtttvn\Orion\Updates;

use Faker;
use Seeder;
use Dmtttvn\Orion\Models\Role;
use Dmtttvn\Orion\Models\Comrade;

class SeedComradesTable extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');

        Comrade::create([
            'role_id'   => 1,
            'name'      => "Дмитрий Орликов",
            'profile'   => $faker->sentence($nbWords = $faker->numberBetween(128, 254)),
        ]);

        for ($i = 0; $i < 30; $i++) { 
            Comrade::create([
                'role_id'   => $faker->numberBetween(2, Role::all()->count()),
                'name'      => "{$faker->firstNameMale} {$faker->lastName}",
                'profile'   => $faker->sentence($nbWords = $faker->numberBetween(128, 254)),
            ]);
        }
    }
}