<?php namespace Dmtttvn\Orion\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDmtttvnOrionBlocks extends Migration
{
    public function up()
    {
        Schema::create('dmtttvn_orion_blocks', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->index();
            $table->integer('project_id')->unsigned()->nullable();
            $table->string('type')->nullable();
            $table->string('title')->nullable();
            $table->text('data')->nullable();
            $table->string('sort_order')->nullable();
            
            $table->foreign('project_id')->references('id')->on('dmtttvn_orion_projects')->onDelete('cascade');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('dmtttvn_orion_blocks');
    }
}