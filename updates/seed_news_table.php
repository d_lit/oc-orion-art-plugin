<?php namespace Dmtttvn\Orion\Updates;

use Faker;
use Seeder;
use Carbon\Carbon;
use Dmtttvn\Orion\Models\News;

class SeedNewsTable extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');

        $dt = Carbon::now();

        for ($year = 2014; $year <= $dt->year; $year++) {
            $months = ($year == $dt->year) ? $dt->month : 12;
            for ($month = 1; $month <= $months; $month++) {
                for ($day = 1; $day <= 30; $day++) {
                    if ($day % 6 == 5) {
                        News::create([
                            'content'       => $faker->sentence($nbWords = $faker->numberBetween(15,20)),
                            'status'        => News::STATUS_PUBLISHED,
                            'important'     => $faker->numberBetween(1,7) == 7 ? true : false,
                            'published_at'  => Carbon::createFromDate($year, $month, $day),
                        ]);

                    }
                }
            }
        }
    }
}