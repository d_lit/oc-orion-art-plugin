<?php namespace Dmtttvn\Orion\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDmtttvnOrionServices extends Migration
{
    public function up()
    {
        Schema::create('dmtttvn_orion_services', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned()->index();
            $table->string('name')->nullable()->index();
            $table->string('short_name')->nullable()->index();
            $table->string('slug')->nullable()->index();
            $table->text('excerpt')->nullable();
            $table->text('details')->nullable();
            $table->string('status')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('dmtttvn_orion_services');
    }
}