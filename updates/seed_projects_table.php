<?php namespace Dmtttvn\Orion\Updates;

use Faker;
use Seeder;
use Storage;
use Carbon\Carbon;
use System\Models\File;
use Dmtttvn\Orion\Models\Block;
use Dmtttvn\Orion\Models\Project;
use Dmtttvn\Orion\Models\Service;
use Dmtttvn\Orion\Models\Comrade;

class SeedProjectsTable extends Seeder
{
    protected $dtNow;
    protected $faker;

    public function prepareVars()
    {
        $this->dtNow = Carbon::now();
        $this->faker = Faker\Factory::create('ru_RU');
        File::where('attachment_type', 'Dmtttvn\Orion\Models\Block')->orWhere('attachment_type', 'Dmtttvn\Orion\Models\Project')->delete();
    }

    public function run()
    {
        $this->prepareVars();
        $faker = $this->faker;

        for ($year = 2014; $year <= $this->dtNow->year; $year++) {
            $months = ($year == $this->dtNow->year) ? $this->dtNow->month : 12;
            for ($month = 1; $month <= $months; $month++) {
                for ($day = 1; $day <= 30; $day++) {
                    if ($day % 8 == 0) {

                        $project = Project::create([
                            'name'          => "{$faker->country()} – {$year}",
                            'place'         => $faker->city,
                            'description'   => $faker->sentence($faker->numberBetween(5,10)),
                            'status'        => ($faker->numberBetween(1,10) == 10) ? Project::STATUS_DRAFT : Project::STATUS_PUBLISHED,
                            'important'     => ($month % 3 == 0 && $day % 24 == 0) ? true : false,
                            'produced_at'   => Carbon::createFromDate($year, $month, $day, 'Europe/Moscow'),
                            'published_at'  => Carbon::createFromDate($year, $month, $day + $faker->numberBetween(7,21), 'Europe/Moscow'),
                        ]);

                        $photoReportDir = 'media/temp/photos';
                        $comradesPhotos = 'media/temp/comrades';

                        $this->attachRandomImage($project, 'cover', $photoReportDir);
                        $this->linkServices($project);
                        $this->linkComrades($project);

                        $projectFormat  =  ($day % 24 == 0)  ? Project::FORMAT_COMPLEX
                                                            : Project::FORMAT_SIMPLE;

                        switch ($projectFormat) {
                            case Project::FORMAT_SIMPLE:
                                $project->format  = $projectFormat;
                                $project->details = [
                                    'about'    => "<p>{$faker->realText($faker->numberBetween(1200,1500),4)}</p>",
                                    'pluses'   => "{$faker->sentence($faker->numberBetween(20,30))}",
                                    'minuses'  => "{$faker->sentence($faker->numberBetween(20,30))}",
                                    'teamwork' => "<p>{$faker->sentence($faker->numberBetween(30,60))}</p>"
                                ];
                                $project->media = [
                                    'video_code' => '<iframe src="https://player.vimeo.com/video/29568236?color=a3a3a3&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>',
                                ];

                                for ($imageIndex = 1; $imageIndex <= $faker->numberBetween(3,7); $imageIndex++) {
                                    $this->attachRandomImage($project, 'photo_report', $photoReportDir);
                                }

                                for ($imageIndex = 1; $imageIndex <= $faker->numberBetween(2,3); $imageIndex++) {
                                    $this->attachRandomImage($project, 'backstage_photos', $comradesPhotos);
                                }

                                $project->save();
                                break;
                            case Project::FORMAT_COMPLEX:
                                $project->format = $projectFormat;

                                $project->details = [
                                    'teamwork' => "<p>{$faker->sentence($faker->numberBetween(20,40))}</p>"
                                ];

                                $project->media = [
                                    'video_code' => '<iframe src="https://player.vimeo.com/video/29568236?color=a3a3a3&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>',
                                ];

                                $aboutBlock = new Block([
                                    'type'  => Block::TYPE_TEXT,
                                    'title' => Block::$allowedTitleOptions[0],
                                    'data'  => [
                                        'content' => "<p>{$faker->realText($faker->numberBetween(1200,1500),4)}</p>"
                                    ]
                                ]);
                                $project->blocks()->save($aboutBlock);

                                $galleryBlock = new Block([
                                    'type'  => Block::TYPE_GALLERY,
                                    'title' => Block::$allowedTitleOptions[1],
                                    'data'  => [
                                        'attachment_type'  => Block::ATTACHMENT_IMAGE,
                                        'grid_size'        => $faker->numberBetween(3,4),
                                    ],
                                ]);
                                $project->blocks()->save($galleryBlock);

                                for ($imageIndex = 1;
                                    $imageIndex <= $galleryBlock->data['grid_size'] * 2;
                                    $imageIndex++) {
                                    $this->attachRandomImage($galleryBlock, 'images', $photoReportDir);
                                }

                                for ($blockIndex = 0; $blockIndex < $faker->numberBetween(10,12); $blockIndex++) {
                                    if (($blockIndex % 2) == 0) {
                                        $textBlock = $this->createTextBlock();
                                        $project->blocks()->save($textBlock);
                                    }
                                    else {
                                        $galleryBlock = $this->createGalleryBlock(Block::ATTACHMENT_IMAGE);
                                        $project->blocks()->save($galleryBlock);
                                        $rowsNum = $galleryBlock->data['grid_size'] * $faker->numberBetween(1,2);
                                        for ($imageIndex = 1; $imageIndex <= $rowsNum; $imageIndex++) {
                                                $this->attachRandomImage($galleryBlock, 'images', $photoReportDir);
                                        }
                                    }
                                }

                                for ($imageIndex = 1; $imageIndex <= $faker->numberBetween(2,4); $imageIndex++) {
                                    $this->attachRandomImage($project, 'backstage_photos', $comradesPhotos);
                                }

                                $project->save();
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }

    protected function createTextBlock()
    {
        $faker = $this->faker;
        $withTitle = $faker->numberBetween(0, 5) == 5 ? false : true;
        $titles = Block::$allowedTitleOptions;

        $textBlock = new Block([
            'type'  => Block::TYPE_TEXT,
            'title' => $withTitle ? $titles[$faker->numberBetween(2, (count($titles) - 1))] : null,
            'data'  => [
                'content'   => "<p>{$faker->realText($faker->numberBetween(1800,2400),4)}</p>",
                'extra'     => $this->createExtraArray()
            ]
        ]);

        return $textBlock;
    }

    protected function createExtraArray()
    {
        $faker = $this->faker;
        $extra = [];
        $files = Storage::disk('local')->files('media/temp/misc');

        for ($i = 0; $i < $faker->numberBetween(1,2); $i++) { 
            $attachmentType = ($faker->numberBetween(1,10) == 10) ? Block::ATTACHMENT_VIDEO : Block::ATTACHMENT_IMAGE;

            if ($attachmentType == Block::ATTACHMENT_IMAGE) {
                $path = '/temp/misc/' . last(explode('/', $files[$faker->numberBetween(0, count($files) - 1)]));

                $extra[$i] = [
                    'attachment' => [
                        'type'          => Block::ATTACHMENT_IMAGE,
                        'image_url'     => $path,
                        'is_circled'    => true,
                    ],
                    'comment'           => "<p>{$faker->sentence($faker->numberBetween(5,10))}</p>",
                ];
            }
            elseif ($attachmentType == Block::ATTACHMENT_VIDEO) {
                $extra[$i] = [
                    'attachment' => [
                        'type'          => Block::ATTACHMENT_VIDEO,
                        'video_code'    => '<iframe src="https://player.vimeo.com/video/29568236?color=a3a3a3&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>',
                    ],
                    'comment'           => "<p>{$faker->sentence($faker->numberBetween(5,10))}</p>",
                ];
            }
        }
        
        return $extra;
    }

    protected function createGalleryBlock($contentType)
    {
        $faker = $this->faker;
        $galleryBlock = new Block([
            'type'  => Block::TYPE_GALLERY,
            'data'  => [
                'attachment_type'   => $contentType,
                'grid_size'         => $faker->numberBetween(2,4),
            ],
        ]);

        return $galleryBlock;
    }

    protected function attachRandomImage($model, $field, $directory)
    {
        $faker = $this->faker;
        $tempMediaImages = Storage::disk('local')->files($directory);

        $path = storage_path('app/' . $tempMediaImages[
            $faker->numberBetween(0, count($tempMediaImages) - 1)
        ]);

        $attach = new File;
        $attach->title = $faker->sentence($faker->numberBetween(3,5));
        $attach->description = $faker->sentence($faker->numberBetween(10,15));
        $attach->fromFile($path)->save();

        $model->$field()->add($attach);
    }

    protected function linkServices($project)
    {
        $faker = $this->faker;
        $project->services()->attach(
            $faker->randomElements(
                Service::all()->lists('id'),
                $faker->numberBetween(1,Service::all()->count())
            )
        );
    }

    protected function linkComrades($project)
    {
        $faker = $this->faker;
        $project->comrades()->attach(1);
        foreach (Comrade::all() as $comrade) {
            if ($faker->numberBetween(0,1) == 1) {
                $project->comrades()->sync([$comrade->id], false);
            }
        }
    }
}