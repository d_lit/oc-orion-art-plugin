<?php namespace Dmtttvn\Orion\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDmtttvnOrionRoles extends Migration
{
    public function up()
    {
        Schema::create('dmtttvn_orion_roles', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->text('description')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('dmtttvn_orion_roles');
    }
}
