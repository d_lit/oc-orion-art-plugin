<?php namespace Dmtttvn\Orion\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDmtttvnOrionProjectsServices extends Migration
{
    public function up()
    {
        Schema::create('dmtttvn_orion_projects_services', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('project_id')->unsigned();
            $table->integer('service_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('dmtttvn_orion_projects_services');
    }
}
