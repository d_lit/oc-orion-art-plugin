<?php namespace Dmtttvn\Orion\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDmtttvnOrionProjects extends Migration
{
    public function up()
    {
        Schema::create('dmtttvn_orion_projects', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->string('place')->nullable();
            $table->string('description')->nullable();
            $table->text('details')->nullable();
            $table->text('media')->nullable();
            $table->string('format')->nullable();
            $table->string('status')->nullable();
            $table->boolean('important')->default(0);
            $table->date('produced_at')->nullable();
            $table->date('published_at')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('dmtttvn_orion_projects');
    }
}