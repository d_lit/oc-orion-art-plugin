<?php namespace Dmtttvn\Orion\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDmtttvnOrionNews extends Migration
{
    public function up()
    {
        Schema::create('dmtttvn_orion_news', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned()->index();
            $table->text('content')->nullable();
            $table->string('status')->default(0);
            $table->boolean('important')->default(0);
            $table->date('published_at')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('dmtttvn_orion_news');
    }
}