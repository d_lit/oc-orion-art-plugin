<?php namespace Dmtttvn\Orion\Components;

use DB;
use Lang;
use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Dmtttvn\Orion\Models\News as NewsModel;

class News extends ComponentBase
{
    public $year;

    public $years;

    public $news;

    public function componentDetails()
    {
        return [
            'name'        => 'News Component',
            'description' => ''
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->prepareVars();
    }

    protected function prepareVars()
    {
        $this->yearNow = $this->page['yearNow'] = Carbon::now()->year;
        $this->years   = $this->page['years']   = $this->listYears();
        $this->news    = $this->page['news']    = $this->listNews($this->yearNow);
    }

    protected function listYears()
    {
        $newsDbTable = (new NewsModel)->getTable();

        $yearsArray = 
            Db::select("
                SELECT DISTINCT DATE_FORMAT(`published_at`, '%Y') AS `year`
                FROM `{$newsDbTable}`
                ORDER BY `published_at` DESC");

        return array_pluck($yearsArray, 'year');
    }

    protected function listNews($year)
    {
        $news = 
            NewsModel::listFrontEnd(['year' => $year])
            ->get()
            ->groupBy(function($item) {
                return Carbon::parse($item->published_at)->format('M');
            });

        return $news;
    }

    /*
     * Ajax Callback
     */
    public function onGetNews()
    {
        $result = $this->renderPartial('news/_list_news', [
            'yearNow'   => post('previous_year'),
            'news'      => $this->listNews(post('previous_year'))
        ]);

        return [
            'result' => $result,
        ];
    }

}