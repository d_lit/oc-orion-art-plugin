<?php namespace Dmtttvn\Orion\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\CmsCompoundObject;
use Dmtttvn\Orion\Models\Project as ProjectModel;

class Project extends ComponentBase
{
    public $project;


    public function componentDetails()
    {
        return [
            'name'        => 'Project Component',
            'description' => ''
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => '',
                'description' => '',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ],
        ];
    }

    public function onRun()
    {
        $this->project  = $this->page['project']  = $this->loadProject();
        $this->related  = $this->page['related']  = $this->loadRelated();
    }

    protected function loadProject()
    {
        $slug = $this->property('slug');

        $project =
            ProjectModel::
                isPublished()
                ->where('slug', $slug)
                ->first();

        return $project;
    }

    protected function loadRelated()
    {
        $related =
            ProjectModel::
                isPublished()
                ->orderByRaw("RAND()")
                ->take(3)->get();

        return $related;
    }
}