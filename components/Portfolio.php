<?php namespace Dmtttvn\Orion\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Dmtttvn\Orion\Models\Project as ProjectModel;
use Dmtttvn\Orion\Models\Service as ServiceModel;

class Portfolio extends ComponentBase
{
    public $page;
    public $services;
    public $service;
    public $projects;

    public function componentDetails()
    {
        return [
            'name'        => 'Portfolio Component',
            'description' => ''
        ];
    }

    public function defineProperties()
    {
        return [];
    }


    public function onRun()
    {
        $this->services = $this->page['services'] = $this->listServices();
        $this->service  = $this->page['service']  = $this->loadService();
        $projects = $this->listProjects();
        $sortedProjects = $this->sortProjects($projects);
        $this->projects = $this->page['projects'] = $sortedProjects;
    }

    public function listServices()
    {
        $services = ServiceModel::all();

        return $services;
    }

    public function loadService()
    {
        if (!$serviceFilter = $this->property('serviceFilter'))
            return null;

        if (!$service = ServiceModel::whereSlug($serviceFilter)->first())
            return null;

        return $service;
    }

    public function listProjects()
    {
        $service = $this->service ? $this->service->id : null;

        $projects = ProjectModel::with(['services'])->listFrontEnd([
            'perPage'   => 60,
            'page'      => $this->page || null,
            'service'   => $service
        ]);

        return $projects;
    }

    public function sortProjects($projects)
    {
        $sortedArray = [];
        $completeWeight = $this->page->agent->isDesktop() ? '3' : '2';

        $sum = 0;
        $row = 0;
        $needlePoint = 1;

        foreach ($projects as $project) {
            $weight = $project['important'] == true ? 2 : 1;

            if ($sum + $weight < $completeWeight) {
                $sortedArray[$row][$needlePoint] = $project;
                $sum+= $weight;
                $needlePoint++;
            }
            elseif ($sum + $weight > $completeWeight) {
                $sortedArray[$row + 1][1] = $sortedArray[$row][$needlePoint - 1];
                $sortedArray[$row][$needlePoint - 1] = $project;
                $row++;
                $sum = $sortedArray[$row][1]['important'] == true ? 2 : 1;
                $needlePoint = $sum + 1;
            }
            elseif ($sum + $weight == $completeWeight) {
                $sortedArray[$row][$needlePoint] = $project;
                $sum = 0;
                $row++;
                $needlePoint = 1;
            }
        }

        return $sortedArray;
    }

    //
    // ajax handlers
    //
    public function onGetProjects()
    {
        return;
    }
}