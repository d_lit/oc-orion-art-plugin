<?php namespace Dmtttvn\Orion\Models;

use Model;

/**
 * Model
 */
class Comrade extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * The database table used by the model.
     * @var string
     */
    public $table = 'dmtttvn_orion_comrades';

    /**
     * Disable timestamps by default.
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Validation
     * @var array
     */
    public $rules = [];

    /**
     * Relations
     * @var array
     */
    public $belongsTo = [
        'role'      => ['Dmtttvn\Orion\Models\Role']
    ];
    public $belongsToMany = [
        'projects'          => ['Dmtttvn\Orion\Models\Project', 'table' => 'dmtttvn_orion_projects_comrades'],
        'projects_count'    => ['Dmtttvn\Orion\Models\Project', 'table' => 'dmtttvn_orion_projects_comrades', 'count' => true]
    ];
    public $attachOne = [
        'portrait'  => ['System\Models\File']
    ];
}