<?php namespace Dmtttvn\Orion\Models;

use Lang;
use Model;

/**
 * Model
 */
class Service extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sluggable;

    const STATUS_HIDDEN = 'hidden';
    const STATUS_ACTIVE = 'active';

    /**
     * The database table used by the model.
     * @var string
     */
    public $table = 'dmtttvn_orion_services';

    /**
     * Fillable fields.
     * @var array
     */
    protected $fillable = ['name', 'description', 'content', 'status'];

    /**
     * The accessors to append to the model's array form.
     * @var array
     */
    protected $appends = ['current_status'];

    /**
     * Validation
     * @var array
     */
    public $rules = [];

    /**
     * Generate slugs for these attributes.
     * @var array
     */
    protected $slugs = ['slug' => 'name'];

    /**
     * Disable timestamps by default.
     * @var bool
     */
    public $timestamps = false;

    /**
     * Relations
     * @var array
     */
    public $hasMany = [
        'services'  => ['Dmtttvn\Orion\Models\Service'],
    ];
    public $belongsToMany = [
        'projects'          => [
            'Dmtttvn\Orion\Models\Project',
            'table' => 'dmtttvn_orion_projects_services'
        ],
        'projects_count'    => [
            'Dmtttvn\Orion\Models\Project',
            'table' => 'dmtttvn_orion_projects_services',
            'count' => true
        ],
    ];
    public $attachOne = [
        'cover'     => ['System\Models\File']
    ];

    public function getStatusOptions($keyValue = null)
    {
        return [
            self::STATUS_HIDDEN => Lang::get('dmtttvn.orion::lang.services.fields.status.' . self::STATUS_HIDDEN),
            self::STATUS_ACTIVE => Lang::get('dmtttvn.orion::lang.services.fields.status.' . self::STATUS_ACTIVE),
        ];
    }

    public function getCurrentStatusAttribute()
    {
        if ($this->status) {
            return $this->getStatusOptions()[$this->status];
        }
    }

    public function scopeIsActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function scopeIsHidden($query)
    {
        return $query->where('status', self::STATUS_HIDDEN);
    }

    public function scopeFilterProjects($query, $projects)
    {
        return $query->whereHas('projects', function($q) use ($projects) {
            $q->whereIn('id', $projects);
        });
    }

    public function scopeFilterStatuses($query, $statuses)
    {
        return $query->whereIn('status', $statuses);
    }
}