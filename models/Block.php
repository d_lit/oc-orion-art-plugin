<?php namespace Dmtttvn\Orion\Models;

use Lang;
use Model;

/**
 * Model
 */
class Block extends Model
{
    use \October\Rain\Database\Traits\Validation;

    const TYPE_TEXT     = 'text';
    const TYPE_GALLERY  = 'gallery';
    const TYPE_SLIDER   = 'slider';
    const ATTACHMENT_IMAGE   = 'image';
    const ATTACHMENT_VIDEO   = 'video';
    const EXTRA_SIZE_DEFAULT = 'default';
    const EXTRA_SIZE_LARGE   = 'large';

    /**
     * The database table used by the model.
     * @var string
     */
    public $table = 'dmtttvn_orion_blocks';

    /**
     * Additional list columns
     * @var array
     */
    protected $appends = ['current_type'];

    /**
     * Values are encoded as JSON
     * @var array
     */
    protected $jsonable = ['data'];

    /**
     * Validation
     * @var array
     */
    public $rules = [];

    /*
     * Disable timestamps by default.
     */
    public $timestamps = false;

    /**
     * @var array
     */
    public static $allowedTitleOptions = [
        'О проекте', 'Фоторепортаж', 'Гости', 'Место проведения', 'История', 'Концепт представления', 'Музыка', 'Видеоконтент', 'Перформанс', 'Техническое проектирование'
    ];

    /**
     * Relations
     * @var array
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'project'   => ['Dmtttvn\Orion\Models\Project']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [
        'images' => ['System\Models\File']
    ];

    public function getBlockTypeOptions($keyValue = null)
    {
        return [
            self::TYPE_TEXT  =>
                Lang::get('dmtttvn.orion::lang.blocks.fields.type.' . self::TYPE_TEXT),
            self::TYPE_GALLERY =>
                Lang::get('dmtttvn.orion::lang.blocks.fields.type.' . self::TYPE_GALLERY),
        ];
    }

    public function getAttachmentTypeOptions($keyValue = null)
    {
        return [
            self::ATTACHMENT_IMAGE  =>
                Lang::get('dmtttvn.orion::lang.attachments.' . self::ATTACHMENT_IMAGE),
            self::ATTACHMENT_VIDEO  =>
                Lang::get('dmtttvn.orion::lang.attachments.' . self::ATTACHMENT_VIDEO),
        ];
    }

    public function getGridSizeOptions($keyValue = null)
    {
        return [
            '2' => Lang::get('dmtttvn.orion::lang.blocks.fields.grid.columns.2'),
            '3' => Lang::get('dmtttvn.orion::lang.blocks.fields.grid.columns.3'),
            '4' => Lang::get('dmtttvn.orion::lang.blocks.fields.grid.columns.4'),
        ];
    }

    public function getExtraSizeOptions($keyValue = null)
    {
        return [
            self::EXTRA_SIZE_DEFAULT  =>
                Lang::get('dmtttvn.orion::lang.blocks.fields.extra.size.' . self::EXTRA_SIZE_DEFAULT),
            self::EXTRA_SIZE_LARGE  =>
                Lang::get('dmtttvn.orion::lang.blocks.fields.extra.size.' . self::EXTRA_SIZE_LARGE),
        ];
    }

    public function getCurrentTypeAttribute()
    {
        return $this->getBlockTypeOptions()[$this->type];
    }

    public function setDataAttribute($value)
    {
        $this->attributes['data'] = json_encode(json_decode($value), JSON_UNESCAPED_UNICODE);
    }
}