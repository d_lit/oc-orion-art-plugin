<?php namespace Dmtttvn\Orion\Models;

use Lang;
use Model;
use Carbon\Carbon;

/**
 * Model
 */
class News extends Model
{
    use \October\Rain\Database\Traits\Validation;

    const STATUS_DRAFT = 'draft';
    const STATUS_PUBLISHED = 'published';

    /**
     * The database table used by the model.
     * @var string
     */
    public $table = 'dmtttvn_orion_news';

    /**
     * Disable timestamps by default.
     * @var boolean
     */
    public $timestamps = true;

    /**
     * Fillable fields
     * @var array
     */
    protected $fillable = ['content', 'status', 'important', 'published_at'];

    /**
     * The accessors to append to the model's array form.
     * @var array
     */
    protected $appends = ['current_status'];

    /**
     * Validation
     * @var array
     */
    public $rules = [];

    /**
     * DateTime fields
     * @var array
     */
    public $dates = ['published_at'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getStatusOptions($keyValue = null)
    {
        return [
            self::STATUS_DRAFT => Lang::get('dmtttvn.orion::lang.news.fields.status.' . self::STATUS_DRAFT),
            self::STATUS_PUBLISHED  => Lang::get('dmtttvn.orion::lang.news.fields.status.' . self::STATUS_PUBLISHED),
        ];
    }

    public function getCurrentStatusAttribute()
    {
        if ($this->status) {
            return $this->getStatusOptions()[$this->status];
        }
    }

    public function afterValidate()
    {
        if ($this->status == self::STATUS_PUBLISHED && !$this->published_at) {
            throw new ValidationException([
               'published_at' => 'Пожалуйста, укажите дату публикации.'
            ]);
        }
    }

    public function scopeIsPublished($query)
    {
        return $query->where('status', 'published');
    }

    public function scopeIsDraft($query)
    {
        return $query->where('status', 'draft');
    }

    public function scopeIsImportant($query)
    {
        return $query->where('important', 1);
    }

    public function scopeListFrontEnd($query, $options)
    {
        /*
         * Default options
         */
        extract(array_merge([
            'year'       => null,
            'published'  => true
        ], $options));

        if ($published) {
            $query->isPublished();
        }

        $query->orderBy('published_at', 'desc');

        return $query->whereBetween('published_at', array(
            Carbon::create($year, 1, 1),
            Carbon::create($year, 12, 31)
        ));
    }
}