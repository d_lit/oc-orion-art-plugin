<?php namespace Dmtttvn\Orion\Models;

use Lang;
use Model;
use Carbon\Carbon;
use Dmtttvn\Orion\Models\Service;
use Dmtttvn\Orion\Models\Role;
/**
 * Model
 */
class Project extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sluggable;

    const STATUS_DRAFT = 'draft';
    const STATUS_PUBLISHED = 'published';
    const FORMAT_SIMPLE = 'simple';
    const FORMAT_COMPLEX = 'complex';

    /**
     * @var string The database table used by the model.
     */
    public $table = 'dmtttvn_orion_projects';

    /**
     * Disable timestamps by default.
     * @var boolean
     */
    public $timestamps = true;

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['produced_at'];

    /**
     * Values are encoded as JSON
     * @var array
     */
    protected $jsonable = ['details', 'media'];

    /**
     * The accessors to append to the model's array form.
     * @var array
     */
    protected $appends = ['current_status', 'current_format'];

    /**
     * Validation
     * @var array
     */
    public $rules = [];

    /**
     * Generate slugs for these attributes.
     * @var array
     */
    protected $slugs = ['slug' => 'name'];

    /**
     * Relations
     * @var array
     */
    public $hasMany = [
        'blocks'      => [
            'Dmtttvn\Orion\Models\Block',
        ],
    ];
    public $belongsToMany = [
        'comrades'       => [
            'Dmtttvn\Orion\Models\Comrade',
            'order' => 'role_id asc',
            'table' => 'dmtttvn_orion_projects_comrades',
        ],
        'services'      => [
            'Dmtttvn\Orion\Models\Service',
            'table' => 'dmtttvn_orion_projects_services'
        ]
    ];
    public $attachOne = [
        'cover' => ['System\Models\File'],
    ];
    public $attachMany = [
        'photo_report' => ['System\Models\File'],
        'backstage_photos' => ['System\Models\File'],
    ];

    /**
     * Lists projects for the front end
     * @param  array $options Display options
     * @return self
     */
    public function scopeListFrontEnd($query, $options)
    {
        extract(array_merge([
            'page'       => 1,
            'perPage'    => 20,
            'sort'       => 'published_at',
            'service'    => null,
            'published'  => true
        ], $options));

        if ($published) {
            $query->isPublished()->where('published_at', '<=', Carbon::now());
        }

        /*
         * Service
         */
        if ($service != null) {
            $service = Service::find($service);
            $query->whereHas('services', function($q) use ($service) {
                $q->whereIn('id', [$service->id]);
            });
        }

        return $query->orderBy($sort, 'desc')->paginate($perPage, $page);
    }

    public function roles()
    {
        $roles = [];

        foreach ($this->comrades as $comrade) {
            $roles[] = $comrade->role;
        }

        return array_unique($roles);
    }

    public function getStatusOptions($keyValue = null)
    {
        return [
            self::STATUS_DRAFT      => Lang::get('dmtttvn.orion::lang.projects.fields.status.' . self::STATUS_DRAFT),
            self::STATUS_PUBLISHED  => Lang::get('dmtttvn.orion::lang.projects.fields.status.' . self::STATUS_PUBLISHED),
        ];
    }

    public function getFormatOptions($keyValue = null)
    {
        return [
            self::FORMAT_SIMPLE => Lang::get('dmtttvn.orion::lang.projects.fields.format.' . self::FORMAT_SIMPLE),
            self::FORMAT_COMPLEX => Lang::get('dmtttvn.orion::lang.projects.fields.format.' . self::FORMAT_COMPLEX),
        ];
    }

    public function getCurrentStatusAttribute()
    {
        if ($this->status) {
            return $this->getStatusOptions()[$this->status];
        }
    }

    public function getCurrentFormatAttribute()
    {
        if ($this->format) {
            return $this->getFormatOptions()[$this->format];
        }
    }

    public function scopeIsPublished($query)
    {
        return $query->where('status', self::STATUS_PUBLISHED);
    }

    public function scopeIsDraft($query)
    {
        return $query->where('status', self::STATUS_DRAFT);
    }

    public function scopeIsSimple($query)
    {
        return $query->where('format', self::FORMAT_SIMPLE);
    }

    public function scopeIsComplex($query)
    {
        return $query->where('format', self::FORMAT_COMPLEX);
    }

    public function scopeIsImportant($query)
    {
        return $query->where('important', true);
    }

    public function scopeFilterServices($query, $services)
    {
        return $query->whereHas('services', function($q) use ($services) {
            $q->whereIn('id', $services);
        });
    }
}