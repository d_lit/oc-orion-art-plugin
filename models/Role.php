<?php namespace Dmtttvn\Orion\Models;

use Model;

/**
 * Model
 */
class Role extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'dmtttvn_orion_roles';

    public $hasMany = [
        'comrades' => [
            'Dmtttvn\Orion\Models\Comrade',
            'order' => 'role_id asc',
            'table' => 'dmtttvn_orion_comrades',
        ]
    ];
}